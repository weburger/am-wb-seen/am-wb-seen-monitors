/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amWbSeenMonitors',[ 
	'ngMaterialWeburgerSeen',//
    'nvd3',//
]);


/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('amWbSeenMonitors')
/**
 * 
 */

.config(['ngMdIconServiceProvider', function(ngMdIconServiceProvider) {
//	ngMdIconServiceProvider
//	// Move actions
//	.addShape('wb-common-moveup', '<path d="M7.41,15.41L12,10.83L16.59,15.41L18,14L12,8L6,14L7.41,15.41Z" />')
//	.addShape('wb-common-movedown', '<path d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z" />')
//	;
}])
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amWbSeenMonitors')
/**
 * @ngdoc module
 * @name ngDonate
 * @description
 * 
 */
.controller('AmWbSeenMonitorStoragePieCtrl', function($scope, $monitor) {



	// TODO: move data to the service
	$scope.chartData = [ {key: 'free', y: 42}, { key: 'used', y: 58} ];
	function updateData(monitor){
		$scope.chartData[0].y=monitor.value;
		$scope.chartData[1].y=monitor.max - monitor.value;
		$scope.chartOptions.chart.title = (monitor.value * 100)/monitor.max +'%';
	}


	$monitor.monitor('tenant', 'storage')//
	.then(function(monitor){
		monitor.watch(function(){
			updateData(monitor);
		});
		return monitor.refresh();
	})//
	.then(function(monitor){
		updateData(monitor)
	});

	$scope.chartOptions = {
			chart: {
				type: 'pieChart',
				height: 210,
				donut: true,
				pie: {
					startAngle: function (d) { return d.startAngle/2 -Math.PI/2 },
					endAngle: function (d) { return d.endAngle/2 -Math.PI/2 }
				},
				x: function (d) { return d.key; },
				y: function (d) { return d.y; },
				valueFormat: (d3.format(".0f")),
				color: ['rgb(0, 150, 136)', 'rgb(191, 191, 191)'],
				showLabels: false,
				showLegend: false,
				tooltips: false,
				title: '42%',
				titleOffset: -10,
				margin: { bottom: -80, left: -20, right: -20 }
			}
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amWbSeenMonitors')
/**
 * @ngdoc module
 * @name AmWbSeenMonitorQueryCtrl
 * @description
 * 
 */
.controller('AmWbSeenMonitorQueryCtrl', function($scope, $monitor) {

	/**
	 * Converts into WB data
	 */
	function toWbData(data){
	
		// vector
		// SEE: https://prometheus.io/docs/querying/api/#range-vectors
		if(data.data.resultType == 'vector'){
			var value = [];
			value[0] = data.data.result[0].value;
			return value;
		}
		
		// matrix
		// SEE: https://prometheus.io/docs/querying/api/#range-vectors
		if(data.data.resultType == 'matrix'){
			return data.data.result[0].values;
		}

		return {};
	}
	
	/**
	 * Converts data into WB metric
	 */
	function toWbMetric(data){
		return data.data.result[0].metric;
	}
	
	/**
	 * Run a query
	 */
	function runQuery(query){
		$monitor.query(query)//
		.then(function(data){
			$scope.value.metric = toWbMetric(data);
			$scope.value.value = toWbData(data);
		});
	}

	/**
	 * Run a range query
	 */
	function runQueryRange(query){
		$monitor.queryRange(query)//
		.then(function(data){
			$scope.value.metric = toWbMetric(data);
			$scope.value.value = toWbData(data);
		});
	}

	// Init scope
	$scope.runQuery = runQuery;
	$scope.runQueryRange = runQueryRange;

});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amWbSeenMonitors')
/**
 * @ngdoc module
 * @name ngDonate
 * @description
 * 
 */
.run(function($resource) {
	$resource.newPage({
		label: 'Monitor query range',
		type: 'monitor-query-range',
		templateUrl: 'views/am-wb-seen-resources/data-monitor-query-range.html',
		controller: 'AmWbSeenMonitorQueryCtrl',
		tags: ['data', 'monitor']
	});
	$resource.newPage({
		label: 'Monitor query',
		type: 'monitor-query',
		templateUrl: 'views/am-wb-seen-resources/data-monitor-query.html',
		controller: 'AmWbSeenMonitorQueryCtrl',
		tags: ['data', 'monitor']
	});
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amWbSeenMonitors')
/**
 * @ngdoc module
 * @name ngDonate
 * @description
 * 
 */
.run(function($settings) {
//	$settings.newPage({
//		type: 'collection',
//		label : 'Collection',
//		description : 'Set collection attribute',
//		icon : 'settings',
//		templateUrl : 'views/am-wb-seen-settings/collection.html'
//	});
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amWbSeenMonitors')
/**
 * @ngdoc module
 * @name ngDonate
 * @description
 * 
 */
.run(function($widget) {
	$widget.newWidget({
		type : 'SeenStoragePie',
		controller : 'AmWbSeenMonitorStoragePieCtrl', 
		templateUrl : 'views/am-wb-seen/monitor-scaler-linechart.html',
		label : 'Seen Storage',
		description : 'Seen storage pie chart',
		icon : 'am-wb-widget-chart',
		link : 'https://gitlab.com/weburger/am-wb-seen-calendar',
		setting : [ ],
		data : {}
	});
});

angular.module('amWbSeenMonitors').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/am-wb-seen-resources/data-monitor-query-range.html',
    "<div layout=row> <form ng-submit=runQueryRange(value._query) name=query id=query-param layout=column> <md-input-container class=md-block> <label translate>Query</label> <input required md-no-asterisk name=query ng-model=value._query.query> <div ng-messages=projectForm.description.$error> <div ng-message=required>This is required.</div> </div> </md-input-container> <md-input-container class=md-block> <label translate>Start</label> <input md-no-asterisk name=start ng-model=value._query.start> <div ng-messages=projectForm.description.$error> <div ng-message=required>This is required.</div> </div> </md-input-container> <md-input-container class=md-block> <label translate>End</label> <input md-no-asterisk name=end ng-model=value._query.end> <div ng-messages=projectForm.description.$error> <div ng-message=required>This is required.</div> </div> </md-input-container> <md-input-container class=md-block> <label translate>Step</label> <input md-no-asterisk name=step ng-model=value._query.step> <div ng-messages=projectForm.description.$error> <div ng-message=required>This is required.</div> </div> </md-input-container> <input type=submit hidden> </form> <div id=query-result layout=column> <hot-table settings=\"{\n" +
    "\t\t\t \tcolHeaders: true, \n" +
    "\t\t\t \tcontextMenu: ['row_above', 'row_below', 'remove_row', 'hsep1', 'col_left', 'col_right', 'hsep2', 'remove_row', 'remove_col', 'hsep3', 'undo', 'redo', 'make_read_only', 'alignment', 'borders'], \n" +
    "\t\t\t \tafterChange: true\n" +
    "\t\t\t }\" row-headers=true min-spare-rows=minSpareRows datarows=value.value height=300 width=500 flex> </hot-table> </div> </div>"
  );


  $templateCache.put('views/am-wb-seen-resources/data-monitor-query.html',
    "<div layout=column> <form ng-submit=runQuery(value._query) name=query id=query-param layout=row flex> <md-input-container class=md-block flex> <label translate>Query</label> <input required md-no-asterisk name=query ng-model=value._query.query> <div ng-messages=projectForm.description.$error> <div ng-message=required>This is required.</div> </div> </md-input-container> <input type=submit hidden> </form> <hot-table settings=\"{\n" +
    "\t\t \tcolHeaders: true, \n" +
    "\t\t \tcontextMenu: ['row_above', 'row_below', 'remove_row', 'hsep1', 'col_left', 'col_right', 'hsep2', 'remove_row', 'remove_col', 'hsep3', 'undo', 'redo', 'make_read_only', 'alignment', 'borders'], \n" +
    "\t\t \tafterChange: true\n" +
    "\t\t }\" row-headers=true min-spare-rows=minSpareRows datarows=value.value height=300 width=500 flex> </hot-table> </div>"
  );


  $templateCache.put('views/am-wb-seen/monitor-scaler-linechart.html',
    "<md-content layout=column layout-align=\"start center\"> <md-toolbar> <h2>Memory usage</h2> </md-toolbar> <p>Here is system memory usage. contact administrator to extend storage size.</p> <nvd3 options=chartOptions data=chartData> </nvd3> </md-content>"
  );

}]);
