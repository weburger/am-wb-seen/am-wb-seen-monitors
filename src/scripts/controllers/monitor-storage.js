/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amWbSeenMonitors')
/**
 * @ngdoc module
 * @name ngDonate
 * @description
 * 
 */
.controller('AmWbSeenMonitorStoragePieCtrl', function($scope, $monitor) {



	// TODO: move data to the service
	$scope.chartData = [ {key: 'free', y: 42}, { key: 'used', y: 58} ];
	function updateData(monitor){
		$scope.chartData[0].y=monitor.value;
		$scope.chartData[1].y=monitor.max - monitor.value;
		$scope.chartOptions.chart.title = (monitor.value * 100)/monitor.max +'%';
	}


	$monitor.monitor('tenant', 'storage')//
	.then(function(monitor){
		monitor.watch(function(){
			updateData(monitor);
		});
		return monitor.refresh();
	})//
	.then(function(monitor){
		updateData(monitor)
	});

	$scope.chartOptions = {
			chart: {
				type: 'pieChart',
				height: 210,
				donut: true,
				pie: {
					startAngle: function (d) { return d.startAngle/2 -Math.PI/2 },
					endAngle: function (d) { return d.endAngle/2 -Math.PI/2 }
				},
				x: function (d) { return d.key; },
				y: function (d) { return d.y; },
				valueFormat: (d3.format(".0f")),
				color: ['rgb(0, 150, 136)', 'rgb(191, 191, 191)'],
				showLabels: false,
				showLegend: false,
				tooltips: false,
				title: '42%',
				titleOffset: -10,
				margin: { bottom: -80, left: -20, right: -20 }
			}
	};
});
